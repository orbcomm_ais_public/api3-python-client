"""example.py
Set a new area on the server, then query for the latest positions of vessels
in the area
"""
from api3_python_client.api_end_points import ApiEndPoints, AreaData, SearchCriteria
from api3_python_client.api_credentials import ApiCredentials
from datetime import datetime, timedelta
from pathlib import Path
import os

###---> This example expects to have the environment variables "USERNAME" and "PASSWORD" set

# Get the username and password from environment variables 
username = os.environ["USERNAME"]
password = os.environ["PASSWORD"]
print(username, password)

LOG_DIRECTORY = Path('test_results/api3_logs')
DOWNLOAD_DIRECTORY = Path('test_results/api3_downloads')

# Create credentials
credentials = ApiCredentials(username=username, password=password, url_path="api/v3_0/")
# Create the ApiEndPoints instance
api = ApiEndPoints(credentials,logDirectory=LOG_DIRECTORY)
# Configure an area in the server
ad = AreaData(name="tg_cherbourg",vertices=[(50,-2),(50,0),(49,0),(49,-2)])
api.login()
list_of_areas = api.put_area(ad)
assert ad.name in list_of_areas
# Execute the query
result = api.execute_query(SearchCriteria(startDate=datetime.utcnow() - timedelta(minutes = 60),
                                        stopDate= datetime.utcnow(),
                                        areaName='tg_cherbourg',
                                        format='json',
                                        forcePending=False),
                            resultDirectory=DOWNLOAD_DIRECTORY,
                        )
print(f"Error Message {result.errorMsg}")
print(f"Filepath {result.filepath}")
print(f"Retrieved data {result.vesselData}")
# Logout
api.logout()