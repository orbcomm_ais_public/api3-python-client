""" Sample code using the API as a context manager """


from api3_python_client.api_credentials import ApiCredentials
from api3_python_client.api_end_points import ApiEndPoints, VesselListData, AreaData, SearchCriteria, ApiAccessError
from pathlib import Path
import os


##----> This example expects to have the environment variables "USERNAME" and "PASSWORD" set

# Get the username and password from environment variables 
username = os.environ["USERNAME"]
password = os.environ["PASSWORD"]
print( username, password)

DIRECTORY_STEM = Path("./test_results")  
LOG_DIRECTORY = DIRECTORY_STEM / "api3_logs"
DOWNLOAD_DIRECTORY = DIRECTORY_STEM / "api3_downloads" 

MMSI_LIST = ["111111111","222222222", "333333333"]
MMSI_LIST_NAME = 'testList105'
AREA_NAME = 'testArea105'



def full_session():
    print("\n******** START FULL SESSION *****************")
    credentials = ApiCredentials(username=username,password=password,url_path="api/v3_0/")
    with ApiEndPoints(credentials=credentials,logDirectory=LOG_DIRECTORY,verify_certificate=True) as ep:
        #----- Login
        try:
            ep.login()
        except ApiAccessError as ex:
            print(f"Login error: {ex.error_text}")
            return

        #----- Add a vessel list
        # It is not necessary nor advised to add the vessel list if it already exists and
        # has not been changed 
        # Addging a list or area counts towards the maximum number of API calls per hours
        print("-----Create vessel list ")
        try:
            vesselList = VesselListData(name=MMSI_LIST_NAME, vessels=MMSI_LIST,type='mmsi_list')
            res = ep.put_vessel_list(vesselList)
        except ApiAccessError as ex:
            print("The vessel list could not be created")
            print(ex.error_text)

        #----- Add an area. 
        # It is not necessary nor advised to add  an area if it already exists and
        # has not been changed 
        # Addging a list or area counts towards the maximum number of API calls per hours
        print("-----Create area ")
        area = AreaData(name=AREA_NAME, vertices=[(40,-20),(40,-10),(30,-10),(30,-20)])
        try:
            ep.put_area(area)
        except( ApiAccessError) as ex:
            print("The area could not be created" )
            print(ex.error_text)

        #----- Execute a query and force it to be a pending query
        # Result is available in the file queryResults.filepath
        print ("------latest position, mmsi_list, json, lookback 1440 minutes, ForcePending")
        criteria = SearchCriteria(format="json", 
                                    latestPosition=True,
                                    listName=MMSI_LIST_NAME,
                                    lookBack=1440,
                                    lookBackUnit='minutes',
                                    forcePending=True )
        queryResults = ep.execute_query(criteria=criteria,resultDirectory=DOWNLOAD_DIRECTORY)
        print(str(queryResults))

        #----- Execute a query without forcing pending. The result is available as a dictionary
        # in queryResults.vesselData
        print ("-----latest position, mmsi_list, json, lookback 1440 minutes, no ForcePending")
        criteria = SearchCriteria(format="json", 
                                    latestPosition=True,
                                    listName=MMSI_LIST_NAME,
                                    lookBack=1440,
                                    lookBackUnit='minutes',
                                    forcePending=False )
        queryResults = ep.execute_query(criteria=criteria,resultDirectory=DOWNLOAD_DIRECTORY)
        print(str(queryResults))
        print(queryResults.vesselData)

        #----- Clean up the account
        # It is not necessary nor advised to clean up the account if the lists or
        # areas will be reused in the future. Removing a list or area counts towards the
        # maximum number of API calls per hours
        try:
            res = ep.del_vessel_list(MMSI_LIST_NAME)
            res = ep.del_area(AREA_NAME)
        except ApiAccessError as ex:
            print("Area or Vessel List could not be deleted")

        #----- Logout
        ep.logout()

def main():
    full_session()


if __name__ == "__main__":
    main()