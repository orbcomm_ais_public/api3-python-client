Getting Started
===============

Requirements
------------

The following requirements are needed

.. csv-table:: Requirements
    :header: "**Component**", "**Version**"
    :widths: 10,10
    :align: left
    :width: 30%

    "Operating System", "Linux, Windows"
    "Python", ">= 3.9.13"
    "Requests Library", ">= 2.28.1"
    "Certify Library", ">= 2022.12.7"



Installation
-------------

**Installation from PyPi**

The library can be installed from PyPi with the command

.. code-block:: bash

    python -m pip install orbcomm_ais_api3-client

:note: Installation from PyPi does not give access to the example file

**Installation from GitLab**

No gitlab account is required to retrieve the files

Install the project from gitlab at https://gitlab.com/orbcomm_ais_public/api3-python-client 


.. code-block:: bash

    git clone https://gitlab.com/orbcomm_ais_public/api3-python-client.git orbcomm_api3_client

The library will be installed in the :file:`orbcomm_api3_client` directory

The library files are in the package :file:`api3_python_client`

An example is in the top level :file:`orbcomm_api3_client` directory


The link to this documentation is https://orbcomm_ais_public.gitlab.io/api3-python-client


High Level Functions
--------------------
The high level functions allows to easily perform a vessel search query. The function takes 
care of all the details of calling the API and processing the results

Step by Step Instructions
^^^^^^^^^^^^^^^^^^^^^^^^^

1. Create a :class:`api_credentials.ApiCredentials` object:

.. code-block:: python

    from api_credentials import ApiCredentials
    credentials = ApiCredentials(username="my_username", password="my_password", url_path="api/v3_0/")) 

2. Create a :class:`api_end_points.ApiEndPoints` object and pass it the previously created credentials:

.. code-block:: python

    from api3_python_client.api_end_points import ApiEndPoints
    api = ApiEndPoints(credentials, logDirectory=Path('./api3_logs'))
    api.login()


3. Create the areas and / or the vessel list desired

Once an area or a list is set of the server, it remains on the server and does
not need to be reentered

.. code-block:: python

    from api3_python_client.api_end_points import AreaData, VesselListData
    # Configure an area in the server
    ad = AreaData(name="tg_cherbourg",vertices=[(50,-2),(50,0),(49,0),(49,-2)])
    list_of_areas = api.put_area(ad)

    # Configure a vessel list in the server
    vld = VesselListData(name="tg_test_list1",type="mmsi_list",vessels=["123456789","987654321"])
    list_of_vessel_list = api.put_vessel_list(vld)
    self.assertTrue(vld.name in list_of_vessel_list)

4. Search for vessel in the area or in the list

.. code-block:: python

    # Search in an area (latest position of each vessel)
    result = api.execute_query(SearchCriteria(startDate=datetime.utcnow() - timedelta(minutes = 60),
                                            stopDate= datetime.utcnow(),
                                            areaName='tg_cherbourg',
                                            format='json',
                                            forcePending=False),
                                resultDirectory=Path('./api3_downloads'),
                            )
    print(f"Error Message {result.errorMsg}")
    print(f"Retrieved data {result.vesselData}")

    # Search for a list (latest position of each vessel)
    result = api.execute_query(SearchCriteria(startDate=datetime.utcnow() - timedelta(minutes = 60),
                                            stopDate= datetime.utcnow(),
                                            listName='tg_test_list1',
                                            format='json',
                                            forcePending=False),
                                resultDirectory=Path('./api3_downloads'),
                            )

    print(f"Error Message {result.errorMsg}")
    print(f"Retrieved data {result.vesselData}")

5. Logout

.. code-block:: python

    api.logout()

Examples
^^^^^^^^

The examples can be run with the shell commands:

.. code-block:: bash

    $ export USERNAME="my_username"
    $ export PASSWORD="my_password"
    $ python example_name.py

.. highlight:: python3

.. literalinclude:: ../../example.py
    :language: python
    :caption: Simple example of creating an area and querying for vessels


.. literalinclude:: ../../example_cm.py
    :language: python
    :caption: Example using the api as a context manager



Low Level Functions
-------------------

The low level functions allows to manually check for query completion
and download the associated file.

:mod:`api3_python_client.api_end_points` provide classes and methods to support this low level functionality

