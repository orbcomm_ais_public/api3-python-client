# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

import datetime
import os
import sys
import sphinx_adc_theme

project = 'API3 Python Client'
copyright = '2023, ORBCOMM'
author = 'ORBCOMM'
release = '1.3.2'
version = '1.3'
today = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S UTC')
#today_fmt = '%Y-%m-%d %H:%M:%S UTC'

sys.path.insert(0,os.path.abspath(os.path.join('..','..')))
print(os.curdir)
print(sys.path)

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration


extensions = [
    "sphinx.ext.napoleon",
    "sphinx.ext.autodoc",
    "sphinx.ext.autosummary",
]

templates_path = ['_templates']
#exclude_patterns = ['__init__.py']
add_modules_names = False
highlight_language = 'python3'
root_doc = 'index'

add_modules_names = False
toc_object_entries_show_parents = 'hide'  # 'domain' 'all'

#-- Options for python --
python_use_unqualified_type_names = True



# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_adc_theme'
html_theme_path = [sphinx_adc_theme.get_html_theme_path()]

html_static_path = ['_static']
