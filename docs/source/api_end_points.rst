api\_end\_points module
=======================



.. automodule:: api3_python_client.api_end_points

    Entity configuration classes
    -----------------------------

    .. autoclass:: AreaData

    .. autoclass:: VesselListData
    
    Search Criteria classes
    -----------------------

    This class is used to define the search criteria

    .. autoclass:: SearchCriteria

        .. automethod:: __str__

    API Functional interface classes
    --------------------------------

    This class provides a functional interface to all the rest API end points.

    .. autoclass:: ApiEndPoints

        Login / Logout Methods
        ^^^^^^^^^^^^^^^^^^^^^^

            .. automethod:: login

            .. automethod:: logout

        Vessel List Management Methods
        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

            .. automethod:: vessel_lists

            .. automethod:: get_vessel_list

            .. automethod:: put_vessel_list

            .. automethod:: del_vessel_list

        Area Management Methods
        ^^^^^^^^^^^^^^^^^^^^^^^

            .. automethod:: area_list

            .. automethod:: get_area

            .. automethod:: put_area

            .. automethod:: del_area

        Vessel Search Methods
        ^^^^^^^^^^^^^^^^^^^^^

            .. automethod:: vessel_search

            .. automethod:: execute_query

        Utility Methods
        ^^^^^^^^^^^^^^^

            .. automethod:: valid_vessel_types

            .. automethod:: valid_vessel_categories

            .. automethod:: validate_search_criteria
            

    Classes returned from API calls
    -------------------------------

    .. autoclass:: ExecuteQueryResult
        :members:
        :special-members: __str__

    .. autoclass:: SearchResult
        :members:

    .. autoclass:: PendingQueryInfo
        :members:

    .. autoclass:: QueryStatus
        :members:
    
    Exception Related Classes
    -------------------------

    .. autoclass:: ApiAccessError

    .. autoclass:: ErrorCategory
        :members:



