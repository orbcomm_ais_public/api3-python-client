.. Api3 Python client documentation master file, created by
   sphinx-quickstart on Mon Jan 16 15:51:50 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Api3 Python client's documentation!
==============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   getting_started
   modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
